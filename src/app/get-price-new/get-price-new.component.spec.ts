import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetPriceNewComponent } from './get-price-new.component';

describe('GetPriceNewComponent', () => {
  let component: GetPriceNewComponent;
  let fixture: ComponentFixture<GetPriceNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetPriceNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetPriceNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
