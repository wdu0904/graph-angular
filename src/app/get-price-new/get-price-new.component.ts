import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Chart } from 'chart.js';
import { interval } from 'rxjs';

@Component({
  selector: 'app-get-price-new',
  templateUrl: './get-price-new.component.html',
  styleUrls: ['./get-price-new.component.css']
})

export class GetPriceNewComponent implements OnInit, AfterViewInit {

	@ViewChild('myCanvas') myCanvas: ElementRef;
	public context: CanvasRenderingContext2D;

  chart = []; //holds chart info
  weatherDates = [];
  closing = [];
  constructor(private http: HttpClient) { }
  
  ngOnInit() {	
	interval(15000).subscribe(x => this.update()); 
  }
  ngAfterViewInit() {
  this.create();}
  
  create(){
  
  var values = [];
  this.http.get('http://incanada1.conygre.com:9080/prices/AAPL?periods=50',{responseType: 'text'}).subscribe(data => {
	var rows = data.split("\n");
      //console.log(rows);
	  for(var i = 1; i < rows.length - 1; i++){
  			var cells = rows[i].split(",");
			var time = cells[0].split(" ");
			var time1 = time[1].split(".");
			this.weatherDates.push(time1[0]);
			this.closing.push(cells[4]);
			values.push({
            "date": time1[0],
            "close": cells[4],
            "open": cells[1],
            "high": cells[2],
            "low": cells[3],
            "volume": cells[5],
            "adjusted": cells[5]
			});
	      }
	  
    
  	          console.log(this.weatherDates);
	          console.log(this.closing);
	          this.context = (<HTMLCanvasElement>this.myCanvas.nativeElement).getContext('2d');
  this.chart = new Chart(this.context, {
          type: 'line',
          data: {
          	labels: this.weatherDates,
            datasets: [
              {
                data: this.closing,
                borderColor: '#3cba9f',
                fill: false
              }
            ]
          },
          options: {
            legend: {
              display: false
            },
            scales: {

              yAxes: [{
                display: true
              }]
            }
          }
        });
	
	});

  }
  
   update(){
  
  var values = [];
  this.http.get('http://incanada1.conygre.com:9080/prices/AAPL?periods=50',{responseType: 'text'}).subscribe(data => {
			var rows = data.split("\n");
	 		var i = rows.length - 2;
  			var cells = rows[i].split(",");
			var time = cells[0].split(" ");
			var time1 = time[1].split(".");
			this.weatherDates.push(time1[0]);
			this.closing.push(cells[4]);
			values.push({
            "date": time1[0],
            "close": cells[4],
            "open": cells[1],
            "high": cells[2],
            "low": cells[3],
            "volume": cells[5],
            "adjusted": cells[5]
			});
	      
	  
	          this.context = (<HTMLCanvasElement>this.myCanvas.nativeElement).getContext('2d');
  this.chart = new Chart(this.context, {
          type: 'line',
          data: {
          	labels: this.weatherDates,
            datasets: [
              {
                data: this.closing,
                borderColor: '#3cba9f',
                fill: false
              }
            ]
          },
          options: {
            legend: {
              display: false
            },
            scales: {

              yAxes: [{
                display: true
              }]
            }
          }
        });
	
	});

  
  }
